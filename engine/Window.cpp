#include "Window.hpp"

Window::Window(const char* title, int width, int height) : title(title), width(width), height(height){}

Window::~Window() = default;
